package com.ait.cars;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/cars")
@Stateless
@LocalBean
public class CarWS {
	
	@EJB
	private CarDAO carDAO;
	
	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	public Response getAllUsers() {
		List<Car> cars = carDAO.getAllCars();
		return Response.status(200).entity(cars).build();
	}
	
	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	@Path("/{id}")
	public Response getCarByID(@PathParam("id") int id) {
		Car car = carDAO.getCarByID(id);
		return Response.status(200).entity(car).build();
	}
	
	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	@Path("/search/{search}")
	public Response getCarByCond(@PathParam("search") String search) {
		List<Car> foundCars = carDAO.getCarsByCond(search);
		return Response.status(200).entity(foundCars).build();
	}

}
