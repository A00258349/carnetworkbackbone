package com.ait.cars;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

@Stateless
@LocalBean
public class CarDAO {
	@PersistenceContext
	private EntityManager em;
	
	public List<Car> getAllCars() {
		Query query = em.createQuery("SELECT c from Car c");
		return query.getResultList();
	}

	public Car getCarByID(int id) {
		return em.find(Car.class, id);
	}

	public List<Car> getCarsByCond(String cond) {
		Query query = em.createQuery("SELECT c from Car c WHERE c.cond=:cond");
		query.setParameter("cond", cond);
		return query.getResultList();
	}
	
}
