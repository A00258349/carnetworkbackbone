//MainView grabs all the rendered subviews (WineViews) and appends them.
var MainView = Backbone.View.extend({
  collection: CarList,
  id: "container",
  initialize: function(){
   this.listenTo(this.collection, 'add', this.renderList);
   this.render();
  },
  
  render: function(){
	console.log(this.collection)
    this.collection.each(function(car){
    	//$('#mainArea').html(new CarDetails({model:car}).render());
    	$('#carList').append(new CarView({model:car}).render());
    }, this);
	var car = this.collection.at(0);
	$('#mainArea').html(new CarDetails({model:car}).render());
  },
  
  
  renderList:function(){
	  $('#carList li').remove();
	  this.collection.each(function(car){
		    $('#carList').append(new CarView({model:c}).render());
  });
 
  }
});

