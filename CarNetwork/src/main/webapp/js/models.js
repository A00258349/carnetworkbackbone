
var rootURL = "http://localhost:8080/CarNetwork/rest/cars";

var Car = Backbone.Model.extend({
	urlRoot:rootURL,
	defaults:{       
		"id":null, 
		"make":"",     
		"model":"",  
		"year":"", 
		"color":"",  
		"litre":"",  
		"mileage":"", 
		"price":"",  
		"image":"",
		"cond":"",
		"seller":""},
  initialize: function(){
    console.log("car init");
  }
});

var CarList = Backbone.Collection.extend({
	model: Car,
	url:rootURL});
