//WineView builds a list item for each wine.
var CarView = Backbone.View.extend({
  model: Car,
  tagName:'li',
  events:{
	  "click li": "alertStatus"
  },
  alertStatus: function(e){
	  var car=this.model;
	  $('#mainArea').html(new CarDetails({model:car}).render());
  },
    render:function(){
		 var template= _.template($('#car_list').html(), this.model.toJSON());
		return this.$el.html(template);
		
  }
});
