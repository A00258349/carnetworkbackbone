
//An instance of MainView is made whenever the app is run.

var mainView;
var carList;
//The rendered main view is put on the page.
$(document).ready(function(){
	//console.log("here");
	carList = new CarList();
	carList.fetch({
		success: function(data){
			console.log(data)
			mainView = new MainView({collection: carList});
			mainView.$el.appendTo(document.body);
		}
	});
});

